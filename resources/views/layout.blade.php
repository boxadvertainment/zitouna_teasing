<!doctype html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9" lang="{{ App::getLocale() }}"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ App::getLocale() }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title'){{ ucfirst(substr(Request::root(), 7, -3)) }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta name="author" content="box.agency" /> -->

    @if (Request::root() == 'http://eddar.tn')
        @include('partials.socialMetaTags', [
            'image' => asset('img/fb-share-manzel.jpg'),
            'title' => 'فلوسك ماشية في الكراء ؟',
            'description' => 'www.Eddar.tn إيجا نهنّيك على الدار على'
        ])
    @elseif (Request::root() == 'http://el3omra.tn')
        @include('partials.socialMetaTags', [
            'image' => asset('img/fb-share-omra.jpg'),
            'title' => 'تحب تفرّح الوالدة ؟',
            'description' => 'www.El3omra.tn إيجا نهنّيك على العمرة على'
        ])
    @elseif (Request::root() == 'http://elkarhba.tn')
        @include('partials.socialMetaTags', [
            'image' => asset('img/fb-share-sayara.jpg'),
            'title' => '؟ Transport تعبْت مِلْـ',
            'description' => 'www.ElKarhba.tn إيجا نهنّيك على الكرهبة على'
        ])
    @elseif (Request::root() == 'http://elmosta9bel.tn')
        @include('partials.socialMetaTags', [
            'image' => asset('img/fb-share-tawfir.jpg'),
            'title' => 'تخمّم على غدوة ؟',
            'description' => 'www.Elmosta9bel.tn إيجا نهنّيك على المستقبل على'
        ])
    @elseif (Request::root() == 'http://elwa9t.tn')
        @include('partials.socialMetaTags', [
            'image' => asset('img/fb-share-tawasol.jpg'),
            'title' => 'طوال عليك الصّف ؟',
            'description' => 'www.Elwa9t.tn إيجا نربّحك الوقت على'
        ])
    @endif

    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    @yield('styles')

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{ asset('js/modernizr.js') }}"></script>
    <script>
        BASE_URL    = '{{ URL::to('/') }}';
        CURRENT_URL = '{{ URL::full() }}';
    </script>
</head>
<body class="@yield('class')">

    <div class="loader">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!--[if lt IE 9]>
    <div class="alert alert-dismissible outdated-browser show" role="alert">
        <h6>Votre navigateur est obsolète !</h6>
        <p>Pour afficher correctement ce site et bénéficier d'une expérience optimale, nous vous recommandons de mettre à jour votre navigateur.
            <a href="http://outdatedbrowser.com/fr" class="update-btn" target="_blank">Mettre à jour maintenant </a>
        </p>
        <a href="#" class="close-btn" title="Fermer" data-dismiss="alert" aria-label="Fermer">&times;</a>
    </div>
    <![endif]-->
    <div class="desktop">
        

        <main class="main" role="main">
            <div class="social-footer">
            </div>
            @yield('content')
        </main>
    </div>

    <div class="mobile">
        <div class="mobile-wrapper">
            <center>
                <img src="img/title/site-mobile-dar.png" class="products-btn eddar-btns">
                <img src="img/title/site-mobile-karehba.png" class="products-btn elkarhba-btns">
                <img src="img/title/site-mobile-mosta9bel.png" class="products-btn elmosta9bel-btns">
                <img src="img/title/site-mobile-omra.png" class="products-btn el3omra-btns">
                <img src="img/title/site-mobile-wa9it.png" class="products-btn elwa9t-btns">
                <br>
                <button class="btn start-btn"> Cliquez içi </button>
                <iframe id="mobile-video" width="1" height="1" frameborder="0" allowfullscreen></iframe>
            </center>
        </div>
    </div>

    @yield('body')

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ asset('js/jquery.min.js') }}"><\/script>')</script>

    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>

    @yield('scripts')

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-67512881-1', 'auto');
      ga('send', 'pageview', {
          'page': '{{ Request::root() }}',
          'title': '{{ Request::root() }}'
        });

    </script>
</body>
</html>
