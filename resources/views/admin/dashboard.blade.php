<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Deyson Bejarano">
    <title>Admin Panel - 5ans Zitouna</title>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">

<body>
<div id="snippetContent" style="padding-top:10px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-10"><h1>5ans Zitouna</h1></div>
        </div>
        <hr>
        <div class="alert alert-info" role="alert">
            Le nombre total des participants : <strong>{{ $count['total'] }}</strong> <br/>
        </div>
        <br/>
        <div class="row">
            <div class="col-sm-3"><!--left col-->

                <div class="list-group">
                    <a href="{{ route('admin.dashboard') }}?site=http://eddar.tn&username=zitouna" class="list-group-item @if($site == 'http://eddar.tn') active @endif">eddar.tn <span class="badge">{{ $count['eddar'] }}</span></a>
                    <a href="{{ route('admin.dashboard') }}?site=http://elkarhba.tn&username=zitouna" class="list-group-item @if($site == 'http://elkarhba.tn') active @endif">elkarhba.tn <span class="badge">{{ $count['elkarhba'] }}</span></a>
                    <a href="{{ route('admin.dashboard') }}?site=http://el3omra.tn&username=zitouna" class="list-group-item @if($site == 'http://el3omra.tn') active @endif">el3omra.tn <span class="badge">{{ $count['el3omra'] }}</span></a>
                    <a href="{{ route('admin.dashboard') }}?site=http://elmosta9bel.tn&username=zitouna" class="list-group-item @if($site == 'http://elmosta9bel.tn') active @endif">elmosta9bel.tn <span class="badge">{{ $count['elmosta9bel'] }}</span></a>
                    <a href="{{ route('admin.dashboard') }}?site=http://elwa9t.tn&username=zitouna" class="list-group-item @if($site == 'http://elwa9t.tn') active @endif">elwa9t.tn <span class="badge">{{ $count['elwa9t'] }}</span></a>
                </div>

            </div><!--/col-3-->
            <div class="col-sm-9">

                <div class="tab-content">
                    <div class="tab-pane active" id="home">

                        <br/>
                                <table class="table">
                                    <thead>
                                        <th>#</th>
                                        <th>Nom</th>
                                        <th>Prénom</th>
                                        <th>email</th>
                                        <th>Téléphone</th>
                                        <th>Date de création</th>
                                    </thead>
                                    <tbody>
                                    @foreach($participants as $key => $participant)
                                        <tr>
                                            <td>{{ ($participants->currentPage() - 1) * 10 + $key + 1}}</td>
                                            <td>{{ $participant->name }}</td>
                                            <td>{{ $participant->lastname }}</td>
                                            <td>{{ $participant->email }}</td>
                                            <td>{{ $participant->phone }}</td>
                                            <td>{{ $participant->created_at }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>


                            <div class="text-center">
                                {!! $participants->appends(['username' => 'zitouna', 'site' => $site])->render() !!}
                            </div>

                    </div><!--/tab-pane-->

                </div><!--/tab-pane-->
            </div><!--/tab-content-->

        </div><!--/col-9-->
    </div><!--/row-->
    <style type="text/css">
        body{margin-top:20px;}
    </style>

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ asset('js/jquery.min.js') }}"><\/script>')</script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script> -->

</body>
</html>
