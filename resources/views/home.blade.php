@extends('layout')

@section('class', 'home')

@section('content')
    <div class="pages">
<!--         <div class="page landing-page">
            <div class="content">
                <div class="title-intro"></div>
                <button class="btn start-btn"> Cliquez içi </button>
            </div>
        </div> -->

        <div class="page products-page">
            <video id="products-video" class="video video-js" preload="auto" width="100%" height="100%"></video>
            <div class="content">

                <div class="products-btns el3omra-btns">
                    <button type="button" class="btn products-btn"></button>
                    <button type="button" class="btn products-btn"></button>
                    <button type="button" class="btn products-btn"></button>
                    <button type="button" class="btn products-btn"></button>
                </div>

                <div class="products-btns elkarhba-btns">
                    <button type="button" class="btn products-btn"></button>
                    <button type="button" class="btn products-btn"></button>
                    <button type="button" class="btn products-btn"></button>
                    <button type="button" class="btn products-btn"></button>
                </div>

                <div class="products-btns elmosta9bel-btns">
                </div>

                <div class="products-btns eddar-btns">
                    <button type="button" class="btn products-btn"></button>
                    <button type="button" class="btn products-btn"></button>
                    <button type="button" class="btn products-btn"></button>
                </div>

                <div class="products-btns elwa9t-btns">
                    <button type="button" class="btn products-btn"></button>
                    <button type="button" class="btn products-btn"></button>
                </div>

            </div>
        </div>

        <div class="page form-page">
            <video id="form-video" class="video video-js" preload="auto" width="100%" height="100%" ></video>
            <div class="content">
                <form id="signup-form" class="clearfix" action="{{ action('AppController@signup') }}" method="post">
                    {!! csrf_field() !!}
                    <div class="text">
                        <h1>Inscription</h1>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control input-text" name="lastname" id="name" maxlength="30" placeholder="Nom *" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control input-text" name="name" id="name" maxlength="30" placeholder="Pénom *" required>
                    </div>

                    <div class="form-group">
                        <input type="email" class="form-control input-text" name="email" id="email" placeholder="Email *" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control input-text" name="phone" id="phone" maxlength="8" placeholder="Téléphone *" required>
                    </div>

                    <button type="submit" class="btn submit-form">Valider</button>
                </form>

                <a href="https://www.facebook.com/banque.zitouna.tn" target="_blank" class="fb-page">
                    <img src="{{ asset('img/btn-facebook.png') }}">
                </a>
            </div>
            <div class="btns-services">
                <a href="http://www.eddar.tn?src=reveal" class="service-btn dar"></a>
                <a href="http://www.elkarhba.tn?src=reveal" class="service-btn car"></a>
                <a href="http://www.el3omra.tn?src=reveal" class="service-btn omra"></a>
                <a href="http://www.elmosta9bel.tn?src=reveal" class="service-btn tawfir"></a>
                <a href="http://www.elwa9t.tn?src=reveal" class="service-btn tawasol"></a>
            </div>
        </div>
    </div>
@stop
