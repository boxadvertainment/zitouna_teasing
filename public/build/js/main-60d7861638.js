'use strict';

(function ($) {
  "use strict";

  $('.loader').addClass('hide');
  function showLoader() {
    $('.loader').addClass('hide').removeClass('show');
    $('body').css('overflow', 'hidden');
  }

  function hideLoader() {
    $('.loader').addClass('hide').removeClass('show');
    $('body').css('overflow', 'visible');
  }

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    error: function error(jqXHR) {
      if (/<html>/i.test(jqXHR.responseText)) sweetAlert('Oups!', 'Une erreur serveur s\'est produite, veuillez réessayer ultérieurement.', 'error');else sweetAlert('Oups!', jqXHR.responseText, 'error');
    }
  });

  facebookUtils.init({
    appId: FB_APP_ID,
    scopes: ['email', 'user_friends'],
    requiredScopes: ['email', 'user_friends'],
    targetButton: $('.login-btn')
  });
  function asdf() {
    facebookUtils.login(function (response) {
      if (response.success && response.alreadyExists) {
        return;
      }
      return;
    });
  }
})(jQuery);
//# sourceMappingURL=main.js.map