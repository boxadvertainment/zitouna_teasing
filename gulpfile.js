var gulp        = require('gulp');
var elixir      = require('laravel-elixir');
var browserSync = require('browser-sync').create('My server');

var Task = elixir.Task;

elixir.extend('browserSync', function () {
    /* _____________________________________________________________________________________ */
    // Add these lines to ./node_modules/laravel-elixir/tasks/shared/Css.js
    // after .pipe(gulp.dest(options.output.baseDir))
    // =====================================================================================
    // .pipe(require('browser-sync').get('My server').stream({match: '**/*.css'}))
    /* _____________________________________________________________________________________ */

    var src = [
        'app/**/*',
        'public/**/*',
        'resources/views/**/*',
        '!public/css/*'
    ];

    gulp.task('serve', function () {
        browserSync.init({
            proxy: '5ans-teasing.dev'
        });
    });

    new Task('browserSync', function() {
        if (browserSync.active === true) {
            browserSync.reload();
        }
    }).watch(src);
});

elixir(function(mix) {
    /* _____________________________________________________________________________________ */
    // modify line 91 ./node_modules/laravel-elixir/ingredients/version.js
    // return baseDir + '/build'; with return baseDir;
    /* _____________________________________________________________________________________ */
    mix
        .rubySass('main.scss', 'public/css/main.css')
        .babel([
        //   'facebookUtils.js',
          'main.js'
        ], 'public/js/main.js')
        .scripts([
            'bootstrap/dist/js/bootstrap.min.js',
            'sweetalert/dist/sweetalert.min.js',
            'video.js/dist/video-js/video.js',
            'videojs-youtube/dist/vjs.youtube.js',
            'velocity/velocity.min.js',
            'velocity/velocity.ui.min.js',
        ], 'public/js/vendor.js', 'vendor/bower_components' )
        .styles([
            'bootstrap/dist/css/bootstrap.min.css',
            'sweetalert/dist/sweetalert.css',
            'video.js/dist/video-js/video-js.min.css'
        ], 'public/css/vendor.css', 'vendor/bower_components')
        .copy('vendor/bower_components/modernizr/modernizr.js', 'public/js/modernizr.js')
        .copy('vendor/bower_components/jquery/dist/jquery.min.js', 'public/js/jquery.min.js')
        .copy('vendor/bower_components/video.js/dist/video-js/video-js.swf', 'public/js/video-js.swf')
        .copy('vendor/bower_components/video.js/dist/video-js/font', 'public/font')
        //.version(['css/main.css', 'js/main.js'])
        .browserSync();
});
