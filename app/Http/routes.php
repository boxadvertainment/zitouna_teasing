<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['uses' => 'AppController@home', 'as' => 'home']);
Route::post('signup', 'AppController@signup');
// Route::get('test', ['uses' => 'AppController@test', 'as' => 'test']);

//Route::get('auth/facebookLogin', 'Auth\AuthController@facebookLogin');
/*Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);*/

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    //'middleware' => ['auth', 'authz'],
    //'roles' => ['admin'],
    //'permissions' => ['can_edit']
], function()
{
    Route::get('/', ['uses' => 'AdminController@dashboard', 'as' => 'admin.dashboard']);
});
