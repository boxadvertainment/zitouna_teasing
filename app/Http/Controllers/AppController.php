<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Participant;

class AppController extends Controller
{
    public function home(Request $request)
    {
        if (!$request->input('src') && !\App::environment('local')) {
            return redirect()->away('http://www.5ans.tn/?src=t-d');
        }
        return view('home');
    }

    public function signup(Request $request)
    {
        $validator = \Validator::make( $request->all(), [
            'email' => 'unique:participants,email,NULL,id,referer,' . $request->root(),
            'phone' => 'unique:participants,phone,NULL,id,referer,' . $request->root()
        ]);

        if ( $validator->fails() ) {
            return response()->json(['alreadyExists' => true]);
        }

        $validator = \Validator::make( $request->all(), [
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'phone' => 'regex:/^[0-9]{8}$/'
        ]);

        $validator->setAttributeNames([
            'name' => 'Prénom',
            'lastname' => 'Nom',
            'email' => 'Email',
            'phone' => 'Téléphone',
        ]);

        if ( $validator->fails() ) {
            return response()->json(['success' => false, 'message' => implode('\n', $validator->errors()->all()) ]);
        }

        $participant = new Participant;
        $participant->name = $request->input('name');
        $participant->lastname = $request->input('lastname');
        $participant->email = $request->input('email');
        $participant->phone = $request->input('phone');
        $participant->referer = $request->root();

        if ( $participant->save() ) {
            return response()->json(['success' => true ]);
        }
        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }
}
