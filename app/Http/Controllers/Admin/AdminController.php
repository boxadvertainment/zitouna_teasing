<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Participant;

class AdminController extends Controller
{
    public function dashboard(Request $request)
    {
        if ($request->input('username') != 'zitouna') {
            return response('Unauthorized.', 401);
        }

        $count['total'] = Participant::count();
        $count['eddar'] = Participant::where(['referer' => 'http://eddar.tn'])->count();
        $count['elkarhba'] = Participant::where(['referer' => 'http://elkarhba.tn'])->count();
        $count['el3omra'] = Participant::where(['referer' => 'http://el3omra.tn'])->count();
        $count['elmosta9bel'] = Participant::where(['referer' => 'http://elmosta9bel.tn'])->count();
        $count['elwa9t'] = Participant::where(['referer' => 'http://elwa9t.tn'])->count();

        $site = $request->input('site');

        if ($site) {
            $participants = Participant::where(['referer' => $site])->paginate(10);
        } else {
            $participants = Participant::paginate(10);
        }

        return view('admin.dashboard', ['participants' => $participants, 'site' => $site, 'count' => $count]);
    }

    public function login()
    {
        return true;
    }

}
